import "./App.css";
import {TituloEncabezado} from './components/Titulo';
import {BuscadorEmojis} from './components/Buscador';
import { EmojiList } from "./components/EmojiList";

function App() {
  return (
  <div className="container pt-5">
    <TituloEncabezado/>
    <BuscadorEmojis/>
    <EmojiList datos = {[ {
    "title": "Grinning",
    "symbol": "😀",
    "keywords": "grinning face happy smiley emotion emotion"
  },
  {
    "title": "Grimacing",
    "symbol": "😬",
    "keywords": "grimacing face silly smiley emotion emotion selfie selfie"
  },
  {
    "title": "Grin",
    "symbol": "😁",
    "keywords":
      "grinning face with smiling eyes happy silly smiley emotion emotion good good selfie selfie"
  },
  {
    "title": "Joy",
    "symbol": "😂",
    "keywords":
      "face with tears of joy happy silly smiley cry laugh laugh emotion emotion sarcastic sarcastic"
  }]}/>
       
  </div>
  );
}

export default App;