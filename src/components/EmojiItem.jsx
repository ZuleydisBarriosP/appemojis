export function EmojiItem({ title, symbol, keywords }) {
  return (
    <div className="card text-center">
      <div className="card-header">{title}</div>
      <div className="card-body">
          <h2>{symbol}</h2>
        <h5 className="card-title">Special title treatment</h5>
        <p className="card-text">
         {keywords}
        </p>     
      </div>
    </div>
  );
}
