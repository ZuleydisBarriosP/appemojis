export function BuscadorEmojis() {
  return (
    <div className="row d-flex justify-content-end">
      <div className="col-6">
        <div className="mb-3">
          <label htmlFor="exampleFormControlInput1" className="form-label">
            Buscar Emojis
          </label>
          <input
            type="text"
            className="form-control"
            id="exampleFormControlInput1"
            placeholder="ingrese aqui su emoji"
          />
        </div>
      </div>
    </div>
  );
}
